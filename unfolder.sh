#!/bin/bash

# Copyright released under WTFPL: http://www.wtfpl.net/about/

# Tested on Trisquel GNU-Linux 7 "Belenos"

# a script for 
    # 1) taking a set of music album folders inside an artist folder and turning them into a set of artist-album folders
    # 2) moving the renamed folders to the parent directory (of the artist folder)
    # 3) moving to the parent folder and deleting the empty artist folder
    # 4) replacing any spaces in folders names with underscores
    # 5) replacing any capitals in folder names with corresponding lower case letters
    # 6) adding -[FORMAT] based on the filename extension detected inside the folder (eg -[OGG])

# you can do 1-3 in a terminal on GNU-Linux with:
    # parent=${PWD##*/} ; for album in */ ; do mv "$album" "$parent-$album" ; done ; ls ; mv * ../ ; cd .. / rmdir "$parent"
    # the ls is just to print the renamed folders, to check it worked
# you can do 4 in a terminal on GNU-Linux with:
    # for title in *\ * ; do mv "$title" "${title// /_}" ; done
# you can do 5 in a terminal on GNU-Linux with:
    # for title in */ ; do mv "$title" "${title,,}" ; done
# ideally I want to be able to give a path to this rather than having to be in the folder,
    # and have it check with the user that the rename worked before deleting anything, thus the script

parent=${PWD##*/} # feeds the name of the direct parent folder into a variable called "$parent"

for album in */ ; do 
    #feed each folder inside the folder I’m in, into a variable called $album, until it runs out of folders

    #test #1 (move everything from "echo" onwards to a new line to activate): echo $album #test of the ‘for’ loop; prints the name of each item in the folder

    # mv “$album” “test-$album” # test for rename command; prepend “test-" to the start of each folder name

# moving this out of the for loop, as it only needs to happen once
# parent=${PWD##*/} # feeds the name of the direct parent folder into a variable called “$parent”

mv "$album” “$parent-$album” # prepend the name of the parent folder to the start of the folder name

done #end loop

    #test the renaming worked; print the name of each item in the folder.
    #ls

    # show the user the folder that will be emptied and deleted if the rest of the script runs
echo "$parent"

    # need to propmt the user to type Y or N depending on whether echo outputs the correct obsolete artist folder
    # if N, terminate (and undo?)
    # if Y, carry on

mv * ../ # move everything (folders and files) in the folder to the parent folder (except hidden files)

cd .. # move to parent folder

rmdir "$parent" # deletes obsolete and now empty artist folder

   # changes all spaces to underscores
# for f in *\ * ; do mv "$f" "${f// /_}" ; done

   # change all capitals to lower case
# for title in */ ; do mv "$title" "${title,,}" ; done

   # detect format extension of files inside the folder, add inside square brackets (eg -[MP3] or -[OGG])